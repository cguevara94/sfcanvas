//
//  CanvasAreaSegment.swift
//  Canvas
//
//  Created by Gabriel Guevara on 10/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import Foundation
import UIKit

enum SegmentSeparatorType {
    case Vertical, Horizontal
}

class CanvasSegmentSeparator: NSObject {
    
    var coordinate: CGFloat
    var layersInSegment = [CALayer]()
    var type: SegmentSeparatorType
    var canvasView: UICanvasView
    var segmentArea: CGRect {
        if type == SegmentSeparatorType.Vertical {
            return CGRect(x: coordinate, y: canvasView.canvasView.bounds.minY, width: canvasView.constantCanvasWidth, height: canvasView.canvasView.bounds.height)
        } else if type == SegmentSeparatorType.Horizontal {
            return CGRect(x: canvasView.canvasView.bounds.minX, y: coordinate, width: canvasView.canvasView.bounds.width, height: canvasView.constantCanvasHeight)
        }
        return CGRect()
    }
    
    init(coordinate: CGFloat, type: SegmentSeparatorType, canvasView: UICanvasView) {
        self.coordinate = coordinate
        self.type = type
        self.canvasView = canvasView
    }
    
}
