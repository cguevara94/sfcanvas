//
//  CanvasTapGestureRecognizerBehaivor.swift
//  Canvas
//
//  Created by Gabriel Guevara on 6/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import Foundation
import UIKit

extension UICanvasView {

    func addTapGestureRecognizer (forView: UIView) {
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.delegate = self
        tapGestureRecognizer.addTarget(self, action: #selector(tapAction(tapGesture:)))
        
        forView.isMultipleTouchEnabled = true
        forView.isUserInteractionEnabled = true
        forView.addGestureRecognizer(tapGestureRecognizer)
    }

    func tapAction(tapGesture: UITapGestureRecognizer) {
        switch tapGesture.state {
            case .ended:
                canvasTouchBegan(touch: tapGesture)
                break
            default:
                break
        }
    }
    
}
