//
//  CanvasExpansionBehaivor.swift
//  Canvas
//
//  Created by Gabriel Guevara on 7/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import Foundation
import UIKit

enum Direction: String {
    case left, right, top, down
}

extension UICanvasView {
    
    func updateCanvasSizeArea ( forMovedLayer layer: CALayer ) {
        tryExpansion(forMovedLayer: layer)
        updateLayerSegment(separatorsList: canvasVerticalSegmentsSeparators)
        updateLayerSegment(separatorsList: canvasHorizontalSegmentsSeparators)
        tryContraction()
    }
    
    func tryExpansion(forMovedLayer layer: CALayer) {
        if isLayerOutOfRightBorder(layer: layer) {
            addSegmentSeparator(coordinate: canvasView.bounds.maxX, type: SegmentSeparatorType.Vertical, expantionDirection: .right)
            expandToRight()
            updateCanvasSizeArea(forMovedLayer: layer)
        } else if isLayerOutOfLeftBorder(layer: layer) {
            expandToLeft()
            addSegmentSeparator(coordinate: canvasView.bounds.minX, type: SegmentSeparatorType.Vertical, expantionDirection: .left)
            updateCanvasSizeArea(forMovedLayer: layer)
        } else if isLayerOutOfTopBorder(layer: layer) {
            expandToTop()
            addSegmentSeparator(coordinate: canvasView.bounds.minY, type: SegmentSeparatorType.Horizontal, expantionDirection: .top)
            updateCanvasSizeArea(forMovedLayer: layer)
        } else if isLayerOutOfBottomBorder(layer: layer) {
            addSegmentSeparator(coordinate: canvasView.bounds.maxY, type: SegmentSeparatorType.Horizontal, expantionDirection: .down)
            expandToBottom()
            updateCanvasSizeArea(forMovedLayer: layer)
        }
    }
    
    func tryContraction() {
        if canvasVerticalSegmentsSeparators.count > 0 && removeSegmentSeparator(segment: canvasVerticalSegmentsSeparators.last!) {
            contractFromRight()
            tryContraction()
        } else if canvasVerticalSegmentsSeparators.count > 0 && removeSegmentSeparator(segment: canvasVerticalSegmentsSeparators.first!) {
            contractFromLeft()
            tryContraction()
        } else if canvasHorizontalSegmentsSeparators.count > 0 && removeSegmentSeparator(segment: canvasHorizontalSegmentsSeparators.last!) {
            contractFromBottom()
            tryContraction()
        } else if canvasHorizontalSegmentsSeparators.count > 0 && removeSegmentSeparator(segment: canvasHorizontalSegmentsSeparators.first!) {
            contractFromTop()
            tryContraction()
        }
    }
    
    func updateLayerSegment(separatorsList: [CanvasSegmentSeparator]) {
        for segment in separatorsList {
            if segment.segmentArea.intersects(selectedLayer!.frame) {
                if !segment.layersInSegment.contains(selectedLayer!) {
                    segment.layersInSegment.append(selectedLayer!)
                }
            } else {
                if segment.layersInSegment.contains(selectedLayer!) {
                    segment.layersInSegment.remove(at: segment.layersInSegment.index(of: selectedLayer!)!)
                }
            }
        }
    }
    
    func addSegmentSeparator (coordinate: CGFloat, type: SegmentSeparatorType, expantionDirection: Direction) {
        let newSeparator = CanvasSegmentSeparator(coordinate: coordinate, type: type, canvasView: self)
        if newSeparator.segmentArea.intersects(selectedLayer!.frame) {
            newSeparator.layersInSegment.append(selectedLayer!)
        }
        if type == SegmentSeparatorType.Vertical {
            switch expantionDirection {
                case .right:
                    canvasVerticalSegmentsSeparators.append(newSeparator)
                case .left:
                    canvasVerticalSegmentsSeparators.insert(newSeparator, at: 0)
                default: break
            }
        } else if type == SegmentSeparatorType.Horizontal {
            switch expantionDirection {
                case .down:
                    canvasHorizontalSegmentsSeparators.append(newSeparator)
                case .top:
                    canvasHorizontalSegmentsSeparators.insert(newSeparator, at: 0)
                default: break
            }
        }
    }
    
    func removeSegmentSeparator (segment: CanvasSegmentSeparator) -> Bool {
        if segment.layersInSegment.count == 0 {
            if segment.type == SegmentSeparatorType.Vertical {
                canvasVerticalSegmentsSeparators.remove(at: canvasVerticalSegmentsSeparators.index(of: segment)!)
            } else if segment.type == SegmentSeparatorType.Horizontal {
                canvasHorizontalSegmentsSeparators.remove(at: canvasHorizontalSegmentsSeparators.index(of: segment)!)
            }
            return true
        }
        return false
    }
    
    func expandToRight() {
        canvasView.bounds.size.width += constantCanvasWidth
        canvasViewWidthConstraint.constant = canvasView.bounds.size.width
    }
    
    func contractFromRight() {
        canvasView.bounds.size.width -= constantCanvasWidth
        canvasViewWidthConstraint.constant = canvasView.bounds.size.width
    }
    
    func contractFromBottom() {
        canvasView.bounds.size.height -= constantCanvasHeight
        canvasViewHeightConstraint.constant = canvasView.bounds.size.height
    }
    
    func expandToLeft() {
        canvasView.bounds.origin.x -= constantCanvasWidth
        canvasView.bounds.size.width += constantCanvasWidth
        
        canvasViewWidthConstraint.constant = canvasView.bounds.size.width
    }
    
    func contractFromLeft() {
        canvasView.bounds.origin.x += constantCanvasWidth
        canvasView.bounds.size.width -= constantCanvasWidth
        
        canvasViewWidthConstraint.constant = canvasView.bounds.size.width
    }
    
    func expandToTop() {
        canvasView.bounds.origin.y -= constantCanvasHeight
        canvasView.bounds.size.height += constantCanvasHeight
        canvasViewHeightConstraint.constant = canvasView.bounds.size.height
    }
    
    func contractFromTop() {
        canvasView.bounds.origin.y += constantCanvasHeight
        canvasView.bounds.size.height -= constantCanvasHeight
        canvasViewHeightConstraint.constant = canvasView.bounds.size.height
    }
    
    func expandToBottom() {
        canvasView.bounds.size.height += constantCanvasHeight
        canvasViewHeightConstraint.constant = canvasView.bounds.size.height
    }
    
    func isLayerOutOfRightBorder(layer: CALayer) -> Bool {
        if layer.frame.maxX > canvasView.bounds.maxX {
            return true
        }
        return false
    }

    func isLayerOutOfLeftBorder(layer: CALayer) -> Bool {
        if layer.frame.minX < canvasView.bounds.minX {
            return true
        }
        return false
    }
    
    func isLayerOutOfTopBorder(layer: CALayer) -> Bool {
        if layer.frame.minY < canvasView.bounds.minY {
            return true
        }
        return false
    }
    
    func isLayerOutOfBottomBorder(layer: CALayer) -> Bool {
        if layer.frame.maxY > canvasView.bounds.maxY {
            return true
        }
        return false
    }
    
}
