//
//  CanvasScrollViewBehaivor.swift
//  Canvas
//
//  Created by Gabriel Guevara on 6/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import Foundation
import UIKit

extension UICanvasView: UIScrollViewDelegate {
    
    func updateZoomScale () {
        let contentSize = CGSize(width: canvasView.bounds.size.width + 50, height: canvasView.bounds.size.height + 50)
        canvasScrollView.contentSize = contentSize
        
        let scrollViewFrame = canvasScrollView.frame
        let scaleWidth = scrollViewFrame.size.width / canvasScrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / canvasScrollView.contentSize.height
        let minScale = min(scaleHeight, scaleWidth)
        
        canvasScrollView.minimumZoomScale = minScale / 4
        canvasScrollView.maximumZoomScale = minScale * 4
        canvasScrollView.zoomScale = minScale
        
        centerScrollViewContent()
    }
    
    func centerScrollViewContent() {
        /*let boundsSize = canvasScrollView.bounds.size
        var contentsFrame = canvasView.frame
        
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2
        } else {
            contentsFrame.origin.x = 0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2
        } else {
            contentsFrame.origin.y = 0
        }
        
        canvasView.frame = contentsFrame*/
    }
    
    func updateContentSize() {
        let contentSize = CGSize(width: canvasView.frame.size.width + 75, height: canvasView.frame.size.height + 90)
        canvasScrollView.contentSize = contentSize
    }
    
    func updateScrollViewOffSet () {
        let offsetX = max((canvasScrollView.bounds.width - canvasScrollView.contentSize.width) * 0.5, 0)
        let offsetY = max((canvasScrollView.bounds.height - canvasScrollView.contentSize.height) * 0.5, 0)
        self.canvasScrollView.contentInset = UIEdgeInsetsMake(offsetY, offsetX, 0, 0)
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateContentSize()
        updateScrollViewOffSet()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return canvasView
    }
    
}
