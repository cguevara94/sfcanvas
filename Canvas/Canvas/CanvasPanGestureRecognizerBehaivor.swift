//
//  CanvasPanGestureRecognizerBehaivor.swift
//  Canvas
//
//  Created by Gabriel Guevara on 6/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import Foundation
import UIKit

extension UICanvasView {

    func addPanGestureRecognizer (forView: UIView) {
        let panGestureRecognizer = UIPanGestureRecognizer()
        panGestureRecognizer.delegate = self
        panGestureRecognizer.addTarget(self, action: #selector(dragAction(panGesture:)))
        
        forView.isMultipleTouchEnabled = true
        forView.isUserInteractionEnabled = true
        forView.addGestureRecognizer(panGestureRecognizer)
    }
    
    func dragAction(panGesture: UIPanGestureRecognizer) {
        let location = panGesture.location(in: canvasView)
        switch panGesture.state {
            case .began:
                if selectedLayer != nil {
                    if (selectedLayer?.contains(location))! {
                        isDragLayerForPanGesture = true
                    }
                }
                break
            case .changed:
                if isDragLayerForPanGesture {
                    moveSelectedLayer(toPoint: location)
                }
                break
            case .ended:
                if isDragLayerForPanGesture && selectedLayer != nil {
                    updateCanvasSizeArea(forMovedLayer: selectedLayer!)
                    updateContentSize()
                    updateScrollViewOffSet()
                    updateZoomScale()
                }
                isDragLayerForPanGesture = false
                break
            default:
                break
        }
    }
    
}

extension UICanvasView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
