//
//  CanvasLongPressGestureRecognizerBehaivor.swift
//  Canvas
//
//  Created by Gabriel Guevara on 6/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import Foundation
import UIKit

extension UICanvasView {

    func addLongPressGestureRecognizer (forView: UIView) {
        let longPressGestureRecognizer = UILongPressGestureRecognizer()
        longPressGestureRecognizer.delegate = self
        longPressGestureRecognizer.addTarget(self, action: #selector(longPressAction(longPressGesture:)))
        
        longPressGestureRecognizer.minimumPressDuration = 0.3
        
        forView.isMultipleTouchEnabled = true
        forView.isUserInteractionEnabled = true
        forView.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    func longPressAction(longPressGesture: UILongPressGestureRecognizer) {
        let location = longPressGesture.location(in: canvasView)
        switch longPressGesture.state {
            case .began:
                canvasTouchBegan(touch: longPressGesture)
                break
            case .changed:
                moveSelectedLayer(toPoint: location)
                break
            case .ended:
                if selectedLayer != nil {
                    updateCanvasSizeArea(forMovedLayer: selectedLayer!)
                    updateContentSize()
                    updateScrollViewOffSet()
                    updateZoomScale()
                }
                break
            default:
                break
        }
    }
    
}
