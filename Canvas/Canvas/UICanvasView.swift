//
//  ViewController.swift
//  Canvas
//
//  Created by Gabriel Guevara on 6/6/17.
//  Copyright © 2017 Gabriel Guevara. All rights reserved.
//

import UIKit

class UICanvasView: UIViewController {

    @IBOutlet weak var canvasScrollView: UIScrollView!
    @IBOutlet weak var canvasView: UIView!
    
    @IBOutlet weak var canvasBottom: NSLayoutConstraint!
    @IBOutlet weak var canvasLeading: NSLayoutConstraint!
    @IBOutlet weak var canvasTop: NSLayoutConstraint!
    @IBOutlet weak var canvasTrailing: NSLayoutConstraint!
    
    var selectedLayer: CALayer?
    var isDragLayerForPanGesture = false
    var canvasViewWidthConstraint: NSLayoutConstraint!
    var canvasViewHeightConstraint: NSLayoutConstraint!
    
    var constantCanvasWidth = CGFloat(0.0)
    var constantCanvasHeight = CGFloat(0.0)
    
    var canvasVerticalSegmentsSeparators = [CanvasSegmentSeparator]()
    var canvasHorizontalSegmentsSeparators = [CanvasSegmentSeparator]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        constantCanvasWidth = canvasView.bounds.width
        constantCanvasHeight = canvasView.bounds.height
        
        canvasScrollView.isMultipleTouchEnabled = true
        canvasScrollView.isUserInteractionEnabled = true
        
        canvasScrollView.delegate = self
        
        var segmentSep = CanvasSegmentSeparator(coordinate: 0, type: SegmentSeparatorType.Vertical, canvasView: self)
        canvasVerticalSegmentsSeparators.append(segmentSep)
        segmentSep = CanvasSegmentSeparator(coordinate: 0, type: SegmentSeparatorType.Horizontal, canvasView: self)
        canvasHorizontalSegmentsSeparators.append(segmentSep)
        
        addLayer()
        addLayer2()
        addPanGestureRecognizer(forView: canvasView)
        addTapGestureRecognizer(forView: canvasView)
        addLongPressGestureRecognizer(forView: canvasView)
        
        canvasViewWidthConstraint = NSLayoutConstraint(item: canvasView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute,multiplier: 1, constant: canvasView.bounds.size.width)
        canvasScrollView.addConstraint(canvasViewWidthConstraint)
        
        canvasViewHeightConstraint = NSLayoutConstraint(item: canvasView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute,multiplier: 1, constant: canvasView.bounds.size.height)
        canvasScrollView.addConstraint(canvasViewHeightConstraint)
        
        updateZoomScale()
    }
    
    func addLayer() {
        let layer = CALayer()
        layer.bounds = CGRect(x: canvasView.center.x, y: canvasView.center.y, width: 100, height: 100)
        layer.frame = CGRect(x: canvasView.center.x, y: canvasView.center.y, width: 100, height: 100)
        layer.backgroundColor = UIColor.cyan.cgColor
        layer.name = "MI LAYER"
        canvasView.layer.addSublayer(layer)
        canvasVerticalSegmentsSeparators.first!.layersInSegment.append(layer)
        canvasHorizontalSegmentsSeparators.first!.layersInSegment.append(layer)
    }
    
    func addLayer2() {
        let layer = CALayer()
        layer.bounds = CGRect(x: canvasView.center.x - CGFloat(100), y: canvasView.center.y - CGFloat(100), width: 100, height: 100)
        layer.frame = CGRect(x: canvasView.center.x - CGFloat(100), y: canvasView.center.y - CGFloat(100), width: 100, height: 100)
        layer.backgroundColor = UIColor.yellow.cgColor
        layer.name = "MI LAYER 2"
        canvasView.layer.addSublayer(layer)
        canvasVerticalSegmentsSeparators.first!.layersInSegment.append(layer)
        canvasHorizontalSegmentsSeparators.first!.layersInSegment.append(layer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func lookForLayer(atPoint point: CGPoint) -> CALayer? {
        return lookForLayer(atPoint: point, inLayers: canvasView.layer.sublayers!)
    }
    
    func lookForLayer(atPoint point: CGPoint, inLayers layers: [CALayer]) -> CALayer? {
        for layer in layers {
            if layer.contains(point) {
                return layer
            }
        }
        return nil
    }
    
    func canvasTouchBegan(touch: UIGestureRecognizer) {
        deselectLayer()
        selectedLayer = lookForLayer(atPoint: touch.location(in: canvasView))
        layerDidSelected()
    }
    
    func deselectLayer() {
        if selectedLayer != nil {
            selectedLayer?.backgroundColor = UIColor.cyan.cgColor
            selectedLayer = nil
        }
    }
    
    func layerDidSelected () {
        if selectedLayer != nil {
            selectedLayer?.backgroundColor = UIColor.red.cgColor
        }
    }
    
    func moveSelectedLayer (toPoint point: CGPoint) {
        if selectedLayer != nil {
            selectedLayer?.frame.origin = point
            selectedLayer?.bounds.origin = point
            
            canvasScrollView.panGestureRecognizer.isEnabled = false
            canvasScrollView.panGestureRecognizer.isEnabled = true
        }
    }
    
}
